# lmn-example

### Procedure:

```sh
git clone git@bitbucket.org:developit/lmn-example.git
cd lmn-example

npm i

node .
```

This should install the preset, then log its return value from both the root (`index.js`) and `node_modules/styleguide/index.js` (an example sibling module).